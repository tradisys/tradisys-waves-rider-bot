/* eslint-disable */
import * as Mutations from './mutations-types';
import actions from './actions';

const playedGamesKey = 'played_games_history';

const state = {
  balance: 0,
  gamesCount: 5,
  betAmount: 0.5,
  betRange: 50,
  paused: true,
  playedGames: JSON.parse(localStorage.getItem(playedGamesKey)) || [],
  submittedGames: [],
  leaders: [],
  player: {
    seed: null,
    address: null,
  }
};

const mutations = {
  [Mutations.SET_GAMES_COUNT](state, payload) {
    state.gamesCount = payload;
  },
  [Mutations.SET_BET_RANGE](state, payload) {
    state.betRange = payload;
  },
  [Mutations.SET_BET_AMOUNT](state, payload) {
    state.betAmount = payload;
  },
  [Mutations.SET_PAUSED](state, payload) {
    state.paused = payload;
  },
  [Mutations.SET_LEADERS](state, payload) {
    state.leaders = payload;
  },
  [Mutations.SET_BALANCE](state, payload) {
    state.balance = payload;
  },
  [Mutations.SET_PLAYER_SEED](state, payload) {
    state.player.seed = payload;
  },
  [Mutations.SET_PLAYER_ADDR](state, payload) {
    state.player.address = payload;
  },
  [Mutations.ADD_PLAYED_GAME](state, payload) {
    state.playedGames.push(payload);
    localStorage.setItem(playedGamesKey, JSON.stringify(state.playedGames));
  },
  [Mutations.REM_PLAYED_GAME](state, payload) {
    const index = state.playedGames.findIndex(g => g.id === payload.id);
    if (index > -1) {
      state.playedGames.splice(index, 1);
    }
    localStorage.setItem(playedGamesKey, JSON.stringify(state.playedGames));
  },
  [Mutations.ADD_SUBMITTED_GAME](state, payload) {
    state.submittedGames.push(payload);
  },
  [Mutations.REM_SUBMITTED_GAME](state, payload) {
    const index = state.submittedGames.findIndex(g => g.id === payload.id);
    if (index > -1) {
      state.submittedGames.splice(index, 1);
    }
  },
};

const getters = {
  seed: state => state.player.seed,
  address: state => state.player.address,
  paused: state => state.paused,
  gamesCount: state => state.gamesCount,
  betRange: state => state.betRange,
  betAmount: state => state.betAmount,
  balance: state => state.balance,
  leaders: state => state.leaders,
  playedGames: state => state.playedGames.slice().reverse(),
  submittedGames: state => state.submittedGames.slice().reverse(),
};

export default {
  state,
  actions,
  getters,
  mutations,
};
